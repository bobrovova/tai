const express       = require('express');
const bodyParser    = require('body-parser');
const cookieParser  = require('cookie-parser');
const session       = require('express-session');
const app           = express();
require('dotenv').config();
const logger        = require('./logger');
const port = 8000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cookieParser('abc'));
app.use(session({
    saveUninitialized: true,
    resave: true,
    cookie: { secure: !true },
    secret: 'abc' }
));

app.use(require('./controllers'));

app.listen(port, () => {
    logger.info('Server has been started');
});

module.exports = app;