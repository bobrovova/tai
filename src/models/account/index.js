const bookshelf = require('./../../config/bookshelf');

const Account = bookshelf.Model.extend({
    tableName: 'accounts',
    idAttribute: 'address'
});

module.exports = Account;