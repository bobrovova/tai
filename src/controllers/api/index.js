const _                 = require('underscore');
const lightwallet       = require('eth-lightwallet');
const app               = (require('express').Router)();
const AccountRepository = require('./../../repositories/accounts');
const TxsRepository     = require('./../../repositories/txs');
const logger            = require('./../../logger');
const password          = 'a8asvD23av185kF';

app.get('/createAccount', async (req, res) => {
    lightwallet.keystore.createVault({
        password: password
    }, _.bind(function(err, ks){
        ks.keyFromPassword(password, async function (err, pwDerivedKey) {
            if (err) {
                logger.error(err);

                res.json({
                    error: err
                });

                return;
            }

            ks.generateNewAddress(pwDerivedKey, 1);
            let addr = ks.getAddresses();

            let privateKey = ks.exportPrivateKey(addr[0], pwDerivedKey);

            const account = await AccountRepository.createAccount(addr[0], privateKey);

            if (account.name != undefined && account.name == 'error'){
                res.json({
                    error: account
                })
            } else {
                res.json({
                    address: addr[0],
                })
            }
        });
    }), res);
});

app.get('/setAccount/:address/:private', async (req, res) => {
    const address = req.params.address;
    const privateKey = req.params.private;

    const account = await AccountRepository.createAccount(address, privateKey);

    if (account.name != undefined && account.name == 'error'){
        res.status(503).send(account);
    } else {
        res.json({
            address: address,
        })
    }
});

app.get('/balance/:address', async (req, res) => {
    const address = req.params.address;

    res.json(await AccountRepository.getBalance(address));
});

app.get('/totalSupply', async (req, res) => {
    res.json(await AccountRepository.getTotalSupply());
});

app.get('/transfer/:from/:to/:amount', async (req, res) => {
    const from = req.params.from;
    const to = req.params.to;
    const amount = req.params.amount;

    res.json(await AccountRepository.transfer(from, to, amount));
});

app.get('/status/:hash', async (req, res) => {
    const hash = req.params.hash;

    res.json(await TxsRepository.getStatus(hash));
});

module.exports = app;