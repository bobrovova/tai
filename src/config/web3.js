const Web3      = require('web3');
const config    = require('./app');
const web3      = new Web3(new Web3.providers.HttpProvider(config.nodeAddress));

module.exports = web3;