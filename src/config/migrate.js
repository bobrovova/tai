const DBMigrate = require('db-migrate');
require('dotenv').config();

const dbmigrate = DBMigrate.getInstance();
dbmigrate.run();