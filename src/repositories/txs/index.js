const web3      = require('./../../config/web3');
const logger    = require('./../../logger');

module.exports = {
    getStatus: async function(hash){
        let tx;
        try {
            tx = web3.eth.getTransactionReceipt('0x' + hash);
        } catch (e){
            logger.error(e.toString());

            return {
                error: e.toString()
            }
        }

        if(tx == null){
            return {
                status: 'not found'
            }
        } else if(tx.blockNumber == null){
            return {
                status: 'pending'
            }
        } else if(tx.status == '0x0'){
            return {
                status: 'failed'
            }
        } else if(tx.status == '0x1'){
            return {
                status: 'success'
            }
        }
    }
}