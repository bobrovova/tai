const web3      = require('./../../config/web3');
const tokenABI  = require('./../../config/abi/token.json');
const config    = require('./../../config/app');

module.exports = (function (){
    const tokenInstance = web3.eth.contract(tokenABI);

    return tokenInstance.at(config.tokenAddress);
})();