const Tx                    = require('ethereumjs-tx');
const sprintf               = require('sprintf-js').sprintf;
const aes256                = require('nodejs-aes256');
const tokenInstance         = require('./../token');
const Account               = require('./../../models/account');
const web3                  = require('./../../config/web3');
const addressTo256Hex       = require('./../../utils/address-to-256hex');
const config                = require('./../../config/app');
const logger                = require('./../../logger');
const VALUE                 = web3.toHex(web3.toWei(0, "ether"));

module.exports = {
    createAccount: async function(address, privateKey){
        const account = await (new Account({
            address: address,
            private_key: aes256.encrypt(process.env.CRYPTO_KEY, privateKey)
        })).save(null, {method: 'insert'}).catch(function(error){
            logger.error(error);

            return error;
        });

        return account;
    },
    getBalance: async function(address){
        try {
            const balance = await tokenInstance.balanceOf('0x' + address);

            return balance;
        } catch (e){
            logger.error(e.toString());

            return {
                error: e.toString()
            }
        }
    },
    getTotalSupply: async function(){
        try {
            const totalSupply = tokenInstance.totalSupply();

            return totalSupply;
        } catch (e){
            logger.error(e.toString());

            return {
                error: e.toString()
            }
        }
    },
    transfer: async function(from, to, amount){
        const balanceFrom = await this.getBalance(from);

        if(balanceFrom < amount) {
            return {
                error: "Not enough funds in from address"
            }
        }

        const gasPrice = web3.eth.gasPrice;
        const gasPriceHex = web3.toHex(gasPrice);

        let nonce;

        try {
             nonce = web3.eth.getTransactionCount(0);
        } catch (e){
            logger.error(e.toString());

            return {
                error: e.toString()
            };
        }
        const nonceHex = web3.toHex(nonce);

        let data = '0x905bd5e4' +
            addressTo256Hex(from) +
            addressTo256Hex(to) +
            sprintf('%064d', amount);

        const rawTx = {
            nonce: nonceHex,
            to: config.tokenAddress,
            gas: web3.toHex(80000),
            gasPrice: gasPriceHex,
            value: VALUE,
            data: data,
            from: config.ownerAddress
        };

        const tx = new Tx(rawTx);
        tx.sign(new Buffer(process.env.PRIVATE_KEY_OWNER, 'hex'));
        const serializedTx = tx.serialize();

        try {
            const txHash = web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'));

            return {
                txHash: txHash
            };

        } catch (e){
            logger.error(e.toString());

            return {
                error: e.toString()
            }
        }
    }
};